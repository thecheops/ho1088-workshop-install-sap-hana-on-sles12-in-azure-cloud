#!/bin/bash

## Naming convention
# target and portal = iqn.2019-03.com.suse:hana-s$i
# initiatorName = iqn.2006-04.com.suse:01:hana1-s$i
# targetip=<iSCSI Target node IP address>  #  targetip=10.10.0.7
# make /iscsi directory if it doesn't exist for fileio backstores 
mkdir -p /iscsi

for i in {1..2}; do 
echo "Configuring iSCSI Target using targetcli commands" 
targetcli /backstores/fileio create hana-s$i-sbd /iscsi/hana-s$i-sbd 100MB ## write_back=false
targetcli /iscsi create iqn.2019-03.com.suse:hana-s$i
targetcli /iscsi/iqn.2019-03.com.suse:hana-s$i/tpg1/luns create /backstores/fileio/hana-s$i-sbd
targetcli /iscsi/iqn.2019-03.com.suse:hana-s$i/tpg1/portals create 
## Need this authentication when using targetcli RPM package instead of targetcli-fb ##  Login authentication is default
targetcli /iscsi/iqn.2019-03.com.suse:hana-s$i/tpg1 set attribute authentication=0
targetcli /iscsi/iqn.2019-03.com.suse:hana-s$i/tpg1/acls create iqn.2006-04.com.suse:01:hana1-s$i
targetcli /iscsi/iqn.2019-03.com.suse:hana-s$i/tpg1/acls create iqn.2006-04.com.suse:01:hana2-s$i
done; 
targetcli ls

