{
    "$schema": "https://schema.management.azure.com/schemas/2015-01-01/deploymentParameters.json#",
    "contentVersion": "1.0.0.0",
    "parameters": {
        "location": {
            "value": "eastus"
        },
        "networkInterfaceName": {
            "value": "hana2-s24-nic"
        },
        "enableAcceleratedNetworking": {
            "value": true
        },
        "networkSecurityGroupName": {
            "value": "student24-nsg"
        },
        "networkSecurityGroupRules": {
            "value": [
                {
                    "name": "ssh",
                    "properties": {
                        "priority": 1010,
                        "protocol": "TCP",
                        "access": "Allow",
                        "direction": "Inbound",
                        "sourceApplicationSecurityGroups": [],
                        "destinationApplicationSecurityGroups": [],
                        "sourceAddressPrefix": "*",
                        "sourcePortRange": "*",
                        "destinationAddressPrefix": "*",
                        "destinationPortRange": "22"
                    }
                }
            ]
        },
        "subnetName": {
            "value": "default"
        },
        "virtualNetworkName": {
            "value": "student24-vnet"
        },
        "addressPrefixes": {
            "value": [
                "10.24.0.0/24"
            ]
        },
        "subnets": {
            "value": [
                {
                    "name": "default",
                    "properties": {
                        "addressPrefix": "10.24.0.0/24"
                    }
                }
            ]
        },
        "publicIpAddressName": {
            "value": "hana2-s24-ip"
        },
        "publicIpAddressType": {
            "value": "Dynamic"
        },
        "publicIpAddressSku": {
            "value": "Basic"
        },
        "virtualMachineName": {
            "value": "hana2-s24"
        },
        "virtualMachineRG": {
            "value": "ho1088-rg-student24"
        },
        "osDiskType": {
            "value": "Premium_LRS"
        },
        "dataDisks": {
            "value": [
                {
                    "lun": 0,
                    "createOption": "attach",
                    "caching": "ReadWrite",
                    "writeAcceleratorEnabled": false,
                    "id": null,
                    "name": "hana2-s24_DataDisk_0",
                    "storageAccountType": null,
                    "diskSizeGB": null
                }
            ]
        },
        "dataDiskResources": {
            "value": [
                {
                    "name": "hana2-s24_DataDisk_0",
                    "diskSizeGB": 200,
                    "sku": "Premium_LRS",
                    "creationData": {
                        "createOption": "empty"
                    }
                }
            ]
        },
        "virtualMachineSize": {
            "value": "Standard_DS12_v2"
        },
        "adminUsername": {
            "value": "suse"
        },
        "adminPassword": {
            "value": "susecon2019!"
        },
        "diagnosticsStorageAccountName": {
            "value": "student24sa"
        },
        "diagnosticsStorageAccountId": {
            "value": "Microsoft.Storage/storageAccounts/student24sa"
        },
        "diagnosticsStorageAccountType": {
            "value": "Standard_LRS"
        },
        "diagnosticsStorageAccountKind": {
            "value": "Storage"
        }
    }
}