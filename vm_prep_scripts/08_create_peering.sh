#!/bin/bash

PrimaryVNetId=$(az network vnet show \
  --resource-group ho1088-rg \
  --name ho1088-rg-vnet \
  --query id --out tsv)

for i in {1..1}; do


        /usr/local/bin/az network vnet peering create --resource-group ho1088-rg-student${i} -n student${i}toho1088-rg-vnet --vnet-name student${i}-vnet --remote-vnet-id $PrimaryVNetId --allow-vnet-access;

        StudentVNetId=$(az network vnet show \
        --resource-group ho1088-rg-student${i} \
        --name student${i}-vnet\
        --query id \
        --out tsv)

        /usr/local/bin/az network vnet peering create --resource-group ho1088-rg -n ho1088-rg-vnettostudent${i} --vnet-name ho1088-rg-vnet --remote-vnet-id $StudentVNetId --allow-vnet-access;
done
