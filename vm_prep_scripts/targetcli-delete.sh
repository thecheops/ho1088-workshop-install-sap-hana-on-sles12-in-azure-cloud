#!/bin/bash

## Naming convention
# target and portal = iqn.2019-03.com.suse:hana-s$i
# initiatorName = iqn.2006-04.com.suse:01:hana1-s$i

for i in {1..2}; do 
echo "Configuring iSCSI Target using targetcli commands" 
targetcli /backstores/fileio delete hana-s$i-sbd 
targetcli /iscsi delete iqn.2019-03.com.suse:hana-s$i
targetcli /iscsi/iqn.2019-03.com.suse:hana-s$i/tpg1/luns delete /backstores/fileio/hana-s$i-sbd
done; 
targetcli ls

